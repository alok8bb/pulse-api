export const data = [
  {
    "author_image": "https://i.pravatar.cc/700",
    "author_name": "Alok Pawar",
    "post_type": "question",
    "likes": 1300,
    "text": "What a beautiful day",
    "attachments": [
      {
        "type": "image",
        "url": "image_url",
      },
      {
        "type": "audio",
        "url": "audio_url",
      },
    ],
    "comments": [
      {
        "username": "John Doe",
        "user_image": "https://i.pravatar.cc/700",
        "comment": "random string",
        "likes": 10,
      },
    ],
  },
  {
    "author_image": "https://i.pravatar.cc/700",
    "author_name": "John Smith",
    "post_type": "marketing",
    "likes": 850,
    "text": "Check out our new product line!",
    "attachments": [
      {
        "type": "image",
        "url": "image_url",
      },
      {
        "type": "audio",
        "url": "audio_url",
      },
    ],
    "comments": [
      {
        "username": "Jane Doe",
        "user_image": "https://i.pravatar.cc/700",
        "comment": "Looks amazing!",
        "likes": 5,
      },
      {
        "username": "Mike Johnson",
        "user_image": "https://i.pravatar.cc/700",
        "comment": "I'm interested. Can I get a discount?",
        "likes": 7,
      },
    ],
  },
  {
    "author_image": "https://i.pravatar.cc/700",
    "author_name": "Emily Davis",
    "post_type": "question",
    "likes": 620,
    "text": "Any recommendations for a good book to read?",
    "attachments": [
      {
        "type": "image",
        "url": "image_url",
      },
      {
        "type": "audio",
        "url": "audio_url",
      },
    ],
    "comments": [
      {
        "username": "Sarah Wilson",
        "user_image": "https://i.pravatar.cc/700",
        "comment": "I suggest reading 'The Alchemist' by Paulo Coelho.",
        "likes": 12,
      },
    ],
  },
  {
    "author_image": "https://i.pravatar.cc/700",
    "author_name": "Mark Thompson",
    "post_type": "question",
    "likes": 980,
    "text": "What's your favorite travel destination?",
    "attachments": [
      {
        "type": "image",
        "url": "image_url",
      },
      {
        "type": "audio",
        "url": "audio_url",
      },
    ],
    "comments": [
      {
        "username": "Lisa Anderson",
        "user_image": "https://i.pravatar.cc/700",
        "comment": "I love Bali, Indonesia!",
        "likes": 20,
      },
      {
        "username": "David Johnson",
        "user_image": "https://i.pravatar.cc/700",
        "comment": "For me, it's Paris, France.",
        "likes": 15,
      },
    ],
  },
  {
    "author_image": "https://i.pravatar.cc/700",
    "author_name": "Sophia Roberts",
    "post_type": "marketing",
    "likes": 720,
    "text": "Don't miss out on our limited-time sale!",
    "attachments": [
      {
        "type": "image",
        "url": "image_url",
      },
      {
        "type": "audio",
        "url": "audio_url",
      },
    ],
    "comments": [
      {
        "username": "Michael Davis",
        "user_image": "https://i.pravatar.cc/700",
        "comment": "Great deals! Thanks for sharing.",
        "likes": 8,
      },
      {
        "username": "Jessica Brown",
        "user_image": "https://i.pravatar.cc/700",
        "comment": "I'll definitely check it out.",
        "likes": 6,
      },
    ],
  },
  {
    "author_image": "https://i.pravatar.cc/700",
    "author_name": "Daniel Wilson",
    "post_type": "question",
    "likes": 530,
    "text": "Any recommendations for a good restaurant in town?",
    "attachments": [
      {
        "type": "image",
        "url": "image_url",
      },
      {
        "type": "audio",
        "url": "audio_url",
      },
    ],
    "comments": [
      {
        "username": "Emma Johnson",
        "user_image": "https://i.pravatar.cc/700",
        "comment": "I highly recommend 'The Steak House'.",
        "likes": 14,
      },
    ],
  },
  {
    "author_image": "https://i.pravatar.cc/700",
    "author_name": "Oliver Green",
    "post_type": "marketing",
    "likes": 450,
    "text": "Introducing our latest gadget - the XYZ Smartwatch!",
    "attachments": [
      {
        "type": "image",
        "url": "image_url",
      },
      {
        "type": "audio",
        "url": "audio_url",
      },
    ],
    "comments": [
      {
        "username": "Sophie Anderson",
        "user_image": "https://i.pravatar.cc/700",
        "comment": "Looks stylish and innovative!",
        "likes": 9,
      },
      {
        "username": "Ethan Wilson",
        "user_image": "https://i.pravatar.cc/700",
        "comment": "What's the battery life like?",
        "likes": 6,
      },
    ],
  },
  {
    "author_image": "https://i.pravatar.cc/700",
    "author_name": "Grace Thompson",
    "post_type": "question",
    "likes": 720,
    "text": "What's your favorite movie genre?",
    "attachments": [
      {
        "type": "image",
        "url": "image_url",
      },
      {
        "type": "audio",
        "url": "audio_url",
      },
    ],
    "comments": [
      {
        "username": "Samuel Davis",
        "user_image": "https://i.pravatar.cc/700",
        "comment": "I enjoy action movies.",
        "likes": 13,
      },
      {
        "username": "Emily Wilson",
        "user_image": "https://i.pravatar.cc/700",
        "comment": "I'm a fan of romantic comedies.",
        "likes": 11,
      },
    ],
  },
  {
    "author_image": "https://i.pravatar.cc/700",
    "author_name": "Benjamin Wilson",
    "post_type": "question",
    "likes": 960,
    "text": "What's your favorite hobby?",
    "attachments": [
      {
        "type": "image",
        "url": "image_url",
      },
      {
        "type": "audio",
        "url": "audio_url",
      },
    ],
    "comments": [
      {
        "username": "Sophia Anderson",
        "user_image": "https://i.pravatar.cc/700",
        "comment": "I love painting!",
        "likes": 18,
      },
    ],
  },
  {
    "author_image": "https://i.pravatar.cc/700",
    "author_name": "Lucas Smith",
    "post_type": "marketing",
    "likes": 550,
    "text": "Don't miss our exclusive summer sale!",
    "attachments": [
      {
        "type": "image",
        "url": "image_url",
      },
      {
        "type": "audio",
        "url": "audio_url",
      },
    ],
    "comments": [
      {
        "username": "Olivia Johnson",
        "user_image": "https://i.pravatar.cc/700",
        "comment": "Can't wait to grab some deals!",
        "likes": 7,
      },
      {
        "username": "Mason Anderson",
        "user_image": "https://i.pravatar.cc/700",
        "comment": "I'll spread the word!",
        "likes": 5,
      },
    ],
  },
];
